import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from "@material-ui/core/TextField/TextField";
import Paper from "@material-ui/core/Paper/Paper";
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";
import {addNewPlace} from "../../store/actions/placeActions";
import connect from "react-redux/es/connect/connect";


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    button: {
        margin: theme.spacing.unit,
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
});


class AddPlace extends React.Component {
    state = {
        title: '',
        description: '',
        image: '',
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    fileChangeHandler = event => {
        this.setState({
            image: event.target.files[0]
        });
    };

    editedSectionHandler = event => {
        event.preventDefault();
        const id = this.props.sectionId;
        const Data = new FormData();
        Data.append('image', this.state.image);
        Data.append('title', this.state.title);
        Data.append('description', this.state.description);
        this.props.addNewPlace(Data);
    };

    render() {
        const { classes } = this.props;

        return (
            <Paper className={classes.root} elevation={1}>
                <Typography style={{textAlign: "center"}} variant="headline" component="h3">
                    Создание нового заведения
                </Typography>
            <form
                onSubmit={this.editedSectionHandler}
                className={classes.container} noValidate autoComplete="off">
                <TextField
                    name="title"
                    required
                    id="title"
                    label="Обязательно!"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    placeholder="Название заведения"
                    className={classes.textField}
                    margin="normal"
                    onChange={this.inputChangeHandler}
                />
                <TextField
                    required
                    id="description"
                    name="description"
                    label="Обязательно!"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    placeholder="Описание заведения"
                    fullWidth
                    margin="normal"
                    onChange={this.inputChangeHandler}
                />
                <TextField
                    type={"file"}
                    required
                    id="file"
                    name="image"
                    label="Обязательно!"
                    placeholder="Обложка заведения"
                    margin="normal"
                    onChange={this.fileChangeHandler}
                />
                <Button
                    type="submit"
                    variant="outlined" className={classes.button}>
                    Отправить
                </Button>


            </form>
            </Paper>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addNewPlace: sectionData => dispatch(addNewPlace(sectionData))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(AddPlace));



