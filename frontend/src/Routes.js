import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, withRouter} from 'react-router-dom';

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import SectionsList from "./containers/AllSections/SectionsList";
import CreatePlace from "./containers/CreatePlace/CreatePlace";
import Place from "./containers/Place/Place";


const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/sections" exact component={SectionsList}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            <Route path="/addPlace" exact component={CreatePlace}/>
            <Route path="/place" component={Place}/>
            <Route path="/" component={SectionsList}/>
        </Switch>
    )
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default withRouter(connect(mapStateToProps)(Routes));