import React, {Component} from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import green from "@material-ui/core/colors/green";



class TermsOfUseModal extends Component {
    state = {
        isOpen: true
    };


    handleTermsAgree = () => {
        this.setState({isOpen: false});
        localStorage.setItem('termsOfUse', 'false');
    };

    render() {
        const classes = {
            root: {
                color: green[600],
                '&$checked': {
                    color: green[500],
                },
            },
            checked: {}
        };

        const terms = localStorage.getItem('termsOfUse');
        return (
            terms === 'false' ? null : <Dialog
                open={this.state.isOpen}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">О вашей конфиденциальности</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Здесь будет информация о том, что психологи, пишущие рецензии, не знают никаких ваших данных,
                        только результаты ваших ответов на вопросы. Эта информация никогда не выйдет за пределы сайта.
                        Пожалуйста, отметьте галочку ниже, если вы согласны предоставить нам ваши данные, чтобы мы могли
                        их обработать и помочь.
                    </DialogContentText>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={!this.state.isOpen}
                                onChange={this.handleTermsAgree}
                                classes={{
                                    root: classes.root,
                                    checked: classes.checked,
                                }}
                            />
                        }
                        label="Да, я согласна/согласен предоставить свои данные для обработки."
                    />
                </DialogContent>
            </Dialog>
        )
    };
}

export default TermsOfUseModal;