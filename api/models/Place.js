const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SectionSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    titleImage: {
        type: String,
        required: true
    },
    images: [{
        type: String
    }],
    description: {
        type: String,
        required: true
    },
    rate: [{
        user: {type: Schema.ObjectId, ref: 'User', required: true},
        rateFood: {type: Number, required: true},
        rateService: {type: Number, required: true},
        rateInterior: {type: Number, required: true},
        comment: {type: String, required: true}
    }],
    averageRatingFood: {
        type: Number,
        default: 0
    },
    averageRatingService: {
        type: Number,
        default: 0
    },
    averageRatingInterior: {
        type: Number,
        default: 0
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    passed: {
        type: Number,
        default: 0
    }
});


SectionSchema.methods.calcAverageFood = function (rateArray) {
    const filteredArray = rateArray.filter(obj => {
        if (obj.rateFood !== null) {
            return obj.rateFood;
        }
    });

    const arrayOfRates = filteredArray.map(obj => {
        return obj.rateFood;
    });

    const reducedArray = arrayOfRates.reduce((sum, current) => {
        return sum + current;
    }, 0);

    this.averageRatingFood = reducedArray / arrayOfRates.length;
};

SectionSchema.methods.calcAverageService = function (rateArray) {
    const filteredArray = rateArray.filter(obj => {
        if (obj.rateService !== null) {
            return obj.rateService;
        }
    });

    const arrayOfRates = filteredArray.map(obj => {
        return obj.rateService;
    });

    const reducedArray = arrayOfRates.reduce((sum, current) => {
        return sum + current;
    }, 0);

    this.averageRatingService = reducedArray / arrayOfRates.length;
};

SectionSchema.methods.calcAverageInterior = function (rateArray) {
    const filteredArray = rateArray.filter(obj => {
        if (obj.rateInterior !== null) {
            return obj.rateInterior;
        }
    });

    const arrayOfRates = filteredArray.map(obj => {
        return obj.rateInterior;
    });

    const reducedArray = arrayOfRates.reduce((sum, current) => {
        return sum + current;
    }, 0);

    this.averageRatingInterior = reducedArray / arrayOfRates.length;
};

const Place = mongoose.model('Section', SectionSchema);
module.exports = Place;