import React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";

const AboutUsModal = props => {
	const data = localStorage.getItem("firstVisit");
	return data === "false" ? null : (
		<Dialog
			open={props.isOpen}
			onClose={props.handleModalChange}
			scroll={"body"}
			aria-labelledby="scroll-dialog-title"
		>
			<DialogTitle id="scroll-dialog-title">О чем и зачем этот сайт?</DialogTitle>
			<DialogContent>
				<DialogContentText>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam debitis, distinctio
					excepturi non praesentium! Ab amet atque dignissimos est et ex explicabo impedit labore nam
					necessitatibus nostrum obcaecati officia perspiciatis, possimus reprehenderit, soluta suscipit.
					Aliquid, architecto aspernatur commodi distinctio dolorem eos ipsum, labore molestiae, molestias
					nemo nobis omnis perferendis quae quas quia repellat rerum saepe sequi vero voluptatum. Ab
					accusamus, adipisci amet atque consectetur culpa cumque cupiditate, distinctio dolore dolorem
					dolorum ducimus eius eveniet harum hic inventore ipsam laborum magnam modi mollitia nam nesciunt,
					nisi non nostrum numquam odit qui quod recusandae repellendus reprehenderit repudiandae unde
					veritatis voluptatum. Nostrum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi
					aperiam debitis, distinctio excepturi non praesentium! Ab amet atque dignissimos est et ex explicabo
					impedit labore nam necessitatibus nostrum obcaecati officia perspiciatis, possimus reprehenderit,
					soluta suscipit. Aliquid, architecto aspernatur commodi distinctio dolorem eos ipsum, labore
					molestiae, molestias nemo nobis omnis perferendis quae quas quia repellat rerum saepe sequi vero
					voluptatum. Ab accusamus, adipisci amet atque consectetur culpa cumque cupiditate, distinctio dolore
					dolorem dolorum ducimus eius eveniet harum hic inventore ipsam laborum magnam modi mollitia nam
					nesciunt, nisi non nostrum numquam odit qui quod recusandae repellendus reprehenderit repudiandae
					unde veritatis voluptatum. Nostrum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi
					aperiam debitis, distinctio excepturi non praesentium! Ab amet atque dignissimos est et ex explicabo
					impedit labore nam necessitatibus nostrum obcaecati officia perspiciatis, possimus reprehenderit,
					soluta suscipit. Aliquid, architecto aspernatur commodi distinctio dolorem eos ipsum, labore
					molestiae, molestias nemo nobis omnis perferendis quae quas quia repellat rerum saepe sequi vero
					voluptatum. Ab accusamus, adipisci amet atque consectetur culpa cumque cupiditate, distinctio dolore
					dolorem dolorum ducimus eius eveniet harum hic inventore ipsam laborum magnam modi mollitia nam
					nesciunt, nisi non nostrum numquam odit qui quod recusandae repellendus reprehenderit repudiandae
					unde veritatis voluptatum. Nostrum.
				</DialogContentText>
			</DialogContent>
			<DialogActions>
				<Button onClick={props.handleModalChange} color="primary">
					Хорошо :)
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default AboutUsModal;
