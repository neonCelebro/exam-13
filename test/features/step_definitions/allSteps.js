const urls = require('../urls');
const findAndClick = require('../../common/findAndClick');
const findAndSetValue = require('../../common/findAndSetValue');
const confirm = require('../../common/confirmLoginOrRegister');
const tryClick = require('../../common/TryClick');
const trySetValue = require('../../common/trySetValue');
const confirmExisting = require('../../common/confirmExisting');


module.exports = function () {

    this.Then(/^я разлогинен/, function () {
        tryClick(`//*[@id="root"]/div/header/div/header/div/div/label/span[1]`);
    });
    this.Given(/^я вхожу на сайт как "([^"]*)" c поролем "([^"]*)"$/, function (user, pass) {

        browser.url(urls.loginUrl);

        const loginData = [
            {field: '#emailForm', value: user},
            {field: '#passwordForm', value: pass}
        ];

        loginData.map(item => {
            return trySetValue(item.field, item.value);
        });

        tryClick("//span[text()='Войти']");
        return browser.pause(3000);
    });

    this.Then(/^я создаю новое заведение/, function () {
        tryClick("#create");
        trySetValue("#title", "тестовое заведение");
        trySetValue("#description", "описание тестового заведения");
        browser.chooseFile("#file", "1353516136328.jpg");
        return tryClick("#root > div > main > div > form > button");
    });

    this.Then(/^я вижу это заведение в списке всех заведений/, function () {
        return confirmExisting("//h1[text()[contains(., 'теc')]]");
    });

    this.Then(/^я перехожу на страничку "([^"]*)" и оставляю отзыв с рейтингом/, function (place) {
        confirmExisting(`"//h1[text()[contains(., ${place})]]"`);
        tryClick("//a[@href[contains (., \"/place/Жасмин\")]]");
        tryClick("//*[@id=\"root\"]/div/main/div/div[4]/button");
        tryClick("//span[text()=\"Оставить свой отзыв\"]");
        trySetValue("//textarea", "новый коммент");
        tryClick("body > div:nth-child(3) > div.fade.in.modal > div > div > div.modal-footer > div > span:nth-child(2) > span:nth-child(3)");
        tryClick("body > div:nth-child(3) > div.fade.in.modal > div > div > div.modal-footer > div > span:nth-child(4) > span:nth-child(3)");
        tryClick("body > div:nth-child(3) > div.fade.in.modal > div > div > div.modal-footer > div > span:nth-child(6) > span:nth-child(3)");
        return tryClick("/html/body/div[2]/div[2]/div/div/div[3]/div/button");
    });

    this.Then(/^я вижу что рейтинг изменился/, function () {
        confirmExisting("#root > div > main > div > div:nth-child(2) > div > div.MuiCardContent-root-217 > span > span:nth-child(3) > span > img");

    });

};