import React from 'react';
import Rating from 'react-rating';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import greyStar from '../../../assets/images/grey-star.jpg.png';
import yellowStar from '../../../assets/images/yellow-star.png';
import config from '../../../config';
import {Link} from "react-router-dom";

const styles = {

    container: {
        width: '20%'
    },
    card: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        marginTop: '20px',
        height: '438px'
    },
    enter: {
        color: '#31333d',
        '&:hover': {
            color: "#31333d",
            outline: 'none'
        },
        '&:isActive' : {
            color: "#31333d",
        },
        textDecoration: 'none'
    },
    rate: {
        marginTop: "20px"
    },
    media: {
        height: 0,
        paddingTop: '90%', // 16:9
        backgroundSize: 'contain'
    },
    title: {
        fontWeight: '500',
        fontSize: "20px",
        margin: 0
    },
    text: {
        fontSize: '13px'
    },
    button: {
        marginBottom: '30px',
        fontSize: '12px',
        width: '100%'
    }
};


const Cards = (props) => {

    const {classes} = props;
    const defUrl = `${config.imageUrl}/default.jpg`;
    const url = `${config.imageUrl}/${props.image}`;

    return (
        <div className={classes.container}>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image={props.image !== "" ? url : defUrl}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography className={classes.title} gutterBottom variant="headline" component="h1">
                        {props.title}
                    </Typography>
                    <Typography className={classes.text} component="p">
                        Колличество отзывов: {props.passedAmount}
                    </Typography>
                    <Rating
                        className={classes.rate}
                        initialRating={props.averageRating}
                        readonly="true"
                        emptySymbol={<img src={greyStar} alt=""/>}
                        fullSymbol={<img src={yellowStar} alt=""/>}
                    />
                </CardContent>
                <CardActions style={{display: 'flex', justifyContent: 'flex-end', flexDirection: 'row'}}>
                </CardActions>
                <CardActions style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'row'}}>
                    <Button
                         className={classes.button}
                         size="medium"
                         color="primary"
                    >
                        <Link to={`/place/${props.title}?id=${props.placeId}`} className={classes.enter}>
                        Подробнее
                        </Link>
                    </Button>
                </CardActions>
            </Card>
        </div>
    );
};


export default withStyles(styles)(Cards);