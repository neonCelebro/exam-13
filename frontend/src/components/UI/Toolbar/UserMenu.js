import React, {Component} from 'react';
import IconButton from "@material-ui/core/IconButton/IconButton";
import AccountCircle from '@material-ui/icons/AccountCircle';
import withStyles from "@material-ui/core/styles/withStyles";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class UserMenu extends Component{

    render(){

        return(
            <div>
                <IconButton
                    style={{margin: "0 10px"}}
                    id='menu-appbar'
                    aria-haspopup="true"
                    color="inherit"
                >
                    {this.props.avatar?
                        <Avatar
                        alt="Adelle Charles"
                        src={this.props.avatar}
                    /> :
                        <AccountCircle />}
                </IconButton>
            </div>
        )
    }
}

export default withStyles(styles)(UserMenu)