import React, {Component} from 'react'
import {connect} from "react-redux";
import {Button, FormControl, Modal} from "react-bootstrap";
import Rating from 'react-rating';

import greyStar from '../../../../assets/images/grey-star.jpg.png';
import yellowStar from '../../../../assets/images/yellow-star.png';
import {sendRating} from "../../../../store/actions/placeActions";

class RateModal extends Component {

    state = {
        comment: '',
        rateFood: 0,
        rateInterior: 0,
        rateService: 0
    };

    sendDataHandler = () => {
        const sectionId = this.props.sectionId;
        const userId = this.props.user._id;
        const {rateFood, rateInterior, rateService ,comment} = this.state;

        const ratingData = {
            user: userId,
            rateFood,
            rateInterior,
            rateService,
            comment: comment
        };


        this.props.onSendRate(ratingData, sectionId);
    };

    checkState = () => {
        if(this.state.comment.length <= 0) {
            return true
        } else {
            return false
        }

    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    setRatingFood = (value) => {
        this.setState({rateFood : value});
    };
    setRatingInterior = (value) => {
        this.setState({rateInterior : value});
    };
    setRatingService = (value) => {
        this.setState({rateService : value});
    };

    render() {
        const {comment, rate} = this.state;

        return (
            <Modal
                show={this.props.show}
                onHide={this.props.close}

                bsSize="medium"
                aria-labelledby="contained-modal-title-sm"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-lg">
                        Поделитесь своими впечатлениями
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FormControl
                        bsSize="lg"
                        componentClass="textarea"
                        type="text"
                        name="comment"
                        placeholder="Ваш комментарий"
                        value={comment}
                        onChange={(event) => this.inputChangeHandler(event)}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                        <span style={{marginBottom: '10px'}}>Оцените пожалуйста кухню</span>
                        <Rating
                            onChange={(value) => {
                                this.setRatingFood(value)
                            }}
                            initialRating={this.state.rateFood}
                            emptySymbol={<img src={greyStar} alt=""/>}
                            fullSymbol={<img src={yellowStar} alt=""/>}
                        />
                        <span style={{marginBottom: '10px'}}>Оцените пожалуйста интерьер</span>
                        <Rating
                            name={"rateInterior"}
                            onChange={(value) => {
                                this.setRatingInterior(value)
                            }}
                            initialRating={this.state.rateInterior}
                            emptySymbol={<img src={greyStar} alt=""/>}
                            fullSymbol={<img src={yellowStar} alt=""/>}
                        />
                        <span style={{marginBottom: '10px'}}>Оцените пожалуйста обслуживание</span>
                        <Rating
                            name={"rateService"}
                            onChange={(value) => {
                                this.setRatingService(value)
                            }}
                            initialRating={this.state.rateService}
                            emptySymbol={<img src={greyStar} alt=""/>}
                            fullSymbol={<img src={yellowStar} alt=""/>}
                        />
                        <Button
                            onClick={this.sendDataHandler}
                            style={{marginTop: '20px'}}
                            disabled={this.checkState()}
                        >
                            Далее
                        </Button>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    onSendRate: (ratingData, sectionId) => dispatch(sendRating(ratingData, sectionId))
});

export default connect(mapStateToProps, mapDispatchToProps)(RateModal);