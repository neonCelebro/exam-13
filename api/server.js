const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const users = require('./app/users');
const places = require('./app/places');

app = express();

const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true });

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose connected!');

    app.use('/api/users', users());
    app.use('/api/places', places());

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});





