#language: ru


Функционал: Создание нового заведения
  Сценарий:
    Допустим я разлогинен
    И я вхожу на сайт как "user1@mail.ru" c поролем "user1"
    Если я создаю новое заведение
    То я вижу это заведение в списке всех заведений
    И я разлогинен