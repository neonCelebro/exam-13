import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { getPlaceById} from "../../store/actions/placeActions";
import connect from "react-redux/es/connect/connect";
import config from "../../config";
import greyStar from "../../assets/images/grey-star.jpg.png";
import yellowStar from "../../assets/images/yellow-star.png";
import Rating from "react-rating";
import Button from "@material-ui/core/Button/Button";
import Paper from "@material-ui/core/Paper/Paper";
import RateModal from "../UI/Modals/RateModal/RateModal";

const styles = theme => ({
    card: {
        margin: "0 auto",
        maxWidth: 700,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: -8,
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    button: {
        marginBottom: '30px',
        fontSize: '12px',
        width: '100%'
    }
});

class Place extends React.Component {
    state = {
        expanded: false,
        showRateModal: false,
    };

    componentDidMount (){
        console.log(this.props);
        this.props.onFetchPlaceById(this.props.history.location.search)
    }

    handleOpenRateModal = () => {
        this.setState({showRateModal: !this.state.showRateModal})
    }

    handleExpandClick = () => {
        this.setState(state => ({ expanded: !state.expanded }));
    };

    render() {
        const { classes } = this.props;
        let url;
        this.props.place ?
            url = `${config.imageUrl}/${this.props.place.titleImage}` :
            url = `${config.imageUrl}/default.jpg`;


        return (
            <Card className={classes.card}>
                <CardHeader
                    avatar={
                        <Avatar aria-label="Recipe" className={classes.avatar}>
                            R
                        </Avatar>
                    }

                    title={this.props.place? this.props.place.title: null}
                />
                <CardMedia
                    className={classes.media}
                    image={url}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography component="p">
                        Подкробнее про это заведение:
                    </Typography>
                </CardContent>
                <CardActions className={classes.actions} disableActionSpacing>
                    <Typography
                        paragraph variant="p">
                        Общий рейтинг заведения:
                    <Rating
                        style={{marginLeft: "20px"}}
                        className={classes.rate}
                        initialRating={this.props.place?
                            (this.props.place.averageRatingFood
                            + this.props.place.averageRatingService
                            + this.props.place.averageRatingInterior) / 3
                            : null}
                        readonly="true"
                        emptySymbol={<img src={greyStar} alt=""/>}
                        fullSymbol={<img src={yellowStar} alt=""/>}
                    />
                    </Typography>
                    <IconButton
                        className={classnames(classes.expand, {
                            [classes.expandOpen]: this.state.expanded,
                        })}
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                    <CardContent>
                        <Typography paragraph variant="body2">
                            {this.props.place? this.props.place.description : null}
                        </Typography>
                        <Typography
                            paragraph variant="p">
                            Рейтинг Кухни:
                            <Rating
                                style={{marginLeft: "20px"}}
                                className={classes.rate}
                                initialRating={this.props.place?
                                    this.props.place.averageRatingFood
                                    : null}
                                readonly="true"
                                emptySymbol={<img src={greyStar} alt=""/>}
                                fullSymbol={<img src={yellowStar} alt=""/>}
                            />
                        </Typography>
                            <Typography
                                paragraph variant="p">
                                Рейтинг Обслуживания:
                                <Rating
                                    style={{marginLeft: "20px"}}
                                    className={classes.rate}
                                    initialRating={this.props.place?
                                        + this.props.place.averageRatingService
                                        : null}
                                    readonly="true"
                                    emptySymbol={<img src={greyStar} alt=""/>}
                                    fullSymbol={<img src={yellowStar} alt=""/>}
                                />
                            </Typography>
                                <Typography
                                    paragraph variant="p">
                                    Рейтинг Интерьера:
                                    <Rating
                                        style={{marginLeft: "20px"}}
                                        className={classes.rate}
                                        initialRating={this.props.place?
                                            this.props.place.averageRatingInterior
                                            : null}
                                        readonly="true"
                                        emptySymbol={<img src={greyStar} alt=""/>}
                                        fullSymbol={<img src={yellowStar} alt=""/>}
                                    />
                                </Typography>
                        <Typography paragraph variant="h1">
                            Коментарии к этому заведению:
                                {
                                    this.props.place?
                                        this.props.place.rate.map((rate)=> {
                                            return (
                                                <Paper
                                                    style={{
                                                        margin: "10px",
                                                        padding: "10px",
                                                        boxSizing: "border-box"
                                                    }}
                                                >
                                                    {rate.comment}
                                                </Paper>
                                            )
                                        }) : null
                                }
                        </Typography>
                        {this.props.user?
                            <Button
                            className={classes.button}
                            size="medium"
                            color="primary"
                            onClick={this.handleOpenRateModal}
                        >
                            Оставить свой отзыв
                        </Button> :
                            <Paper component='p'>
                                Войдите в систему, что бы оставить свой комментарий
                            </Paper>
                        }
                    </CardContent>
                </Collapse>
                <RateModal sectionId={this.props.history.location.search} close={this.handleOpenRateModal} show={this.state.showRateModal}/>
            </Card>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    place: state.places.place,
});

const mapDispatchToProps = dispatch => ({
    onFetchPlaceById: id => dispatch(getPlaceById(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Place));