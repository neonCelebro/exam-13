#!/usr/bin/env bash

TEST="$(pwd)"

cd ././../api/
npm run seed:test
pm2 start "yarn test" --name "api-test"
cd ././../frontend/
pm2 start "yarn start:test" --name "front-test"

cd ${TEST}
yarn start --tags @myTest
pm2 kill
