import React, {Component} from 'react';
import {connect} from "react-redux";
import {push} from "react-router-redux";

import {withStyles} from '@material-ui/core/styles';

import {fetchAllSection} from "../../store/actions/placeActions";
import Card from "../../components/UI/Card/Card";
import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import Grid from "@material-ui/core/Grid/Grid";


class SectionsList extends Component {

    state = {
        aboutUsModalIsOpen: true,
        show: false
    };

    componentDidMount() {
        this.props.onFetchAllSection();
    };

    cardHandlerClicked = (id) => {
        if (this.props.user) {
            this.props.route(`/questions?id=${id}`)
        } else {
            this.props.route('/register')
        }
    };

    showModal = () => {
        this.setState({show: true});
    };


    render() {
        return(
            <Grid
                container
                spacing={16}
                alignItems='flex-end'
                justify='space-around'
            >
                {this.props.places && this.props.places.length > 0 ?
            this.props.places.map((place) => {
                return (
                    <Card image={place.titleImage}
                          click={() => this.cardHandlerClicked(place._id)}
                          key={place._id}
                          user={this.props.user}
                          title={place.title}
                          description={place.description}
                          passedAmount={place.passed}
                          averageRating={(place.averageRatingFood
                              + place.averageRatingService
                              + place.averageRatingInterior) /3 }
                          clickModal={this.showModal}
                          placeId={place._id}
                    />)
            }):
            <Paper
            >
            <Typography
                style={{
                    height: "50px",
                    fontSize: "25px",
                    textAlign: "center",
                    verticalAlign: "middle"
                }}
            >
                Пока нет ни одного заведения, У тебя есть шанс быть первым!))
            </Typography>
            </Paper>
                }
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    places: state.places.places,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    route: path => dispatch(push(path)),
    onFetchAllSection: () => dispatch(fetchAllSection())
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles()(SectionsList));