import {push} from "react-router-redux";

import axios from '../../axios';
import {
    ADD_NEW_SECTION_SUCCESS,
    FETCH_ALL_SECTIONS_ERROR,
    FETCH_ALL_SECTIONS_SUCCESS,
    ADD_NEW_SECTION_FAILURE, FETCH_PLACE_BY_ID,
} from "./actionTypes";
import {NotificationManager} from "react-notifications";

const addNewSectionSuccess = section => ({
    type: ADD_NEW_SECTION_SUCCESS, section
});

const addNewSectionFailure = error => {
    return {type: ADD_NEW_SECTION_FAILURE, error};
};

export const addNewPlace = (Data) => {
    return dispatch => {
        return axios.post('/places', Data).then(response => {
            dispatch(addNewSectionSuccess(response.data));
            dispatch(push("/"));
            NotificationManager.success('Секция успешно создана');
        }, error => {
            console.log(error.response);
            dispatch(addNewSectionFailure(error.response.data.message));
            NotificationManager(error.response.data.message);
        })
    }
};

export const fetchPlaceById = place => ({
    type: FETCH_PLACE_BY_ID, place
});

export const fetchAllSectionsSuccess = allSections => ({
    type: FETCH_ALL_SECTIONS_SUCCESS, allSections
});

const fetchAllSectionsError = error => ({
    type: FETCH_ALL_SECTIONS_ERROR, error
});

export const fetchAllSection = () => {
    return dispatch => {
        return axios.get('/places').then(response => {
            dispatch(fetchAllSectionsSuccess(response.data));
        }, error => {
            dispatch(fetchAllSectionsError(error.response.data.message));
            NotificationManager.error(error.response.data.message);
        })
    }
};
//
//
//
export const sendRating = (ratingData, sectionId) => {
    return dispatch => {
        return axios.post(`/places/rate${sectionId}`, {ratingData}).then(response => {
            dispatch(push('/'));
        }, error => {
            NotificationManager.error(error.response.data.message);
        })
    }
};
//
//
export const getPlaceById = (id) => {
    return dispatch => {
        return axios.get(`/places/place${id}`).then(response => {
            dispatch(fetchPlaceById(response.data));
        }, (error) => {
            NotificationManager.error('Произошла ошибка при загрузке заведения!');
            dispatch(push("/"))
        })
    }
};
