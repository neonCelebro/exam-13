const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Place = require('./models/Place');



mongoose.connect(`${config.db.url}/places`, { useNewUrlParser: true });

const db = mongoose.connection;

const dropCollection = async (collectionName) => {
    try {
        await db.dropCollection(collectionName);
    } catch (e) {
        console.log(`Collection ${collectionName} did not present, skipping drop...`)
    }
};

const collections = ['users', 'sections'];

db.once('open', async () => {
    collections.forEach(collectionName => (
        dropCollection(collectionName)
    ));

    const [user, admin] = await User.create({
        email: 'user1@mail.ru',
        displayName: 'Обычный пользователь',
        facebookId: '123',
        vkontakteId: '123',
        password: 'user1',
        role: 'admin',
        token: ''
    }, {
        email: 'user2@mail.ru',
        displayName: 'Админ',
        facebookId: '132',
        vkontakteId: '310072015',
        password: 'user2',
        role: 'admin',
        token: ''
    });


    await Place.create(
        {
            title: 'Жасмин',
            titleImage: "default.jpg",
            isActive: true,
            description: 'Description for section #1',
            rate: [
                {
                    user: user._id,
                    rateFood: 2,
                    rateInterior: 3,
                    rateService: 2,
                    comment: "some new comment"
                }
            ],
            images: [],
            averageRatingFood: 2,
            averageRatingService: 2,
            averageRatingInterior: 2,
            isDeleted: false,
            passed: 0,

        });

    db.close();
})
;