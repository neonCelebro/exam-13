import React, { Component } from "react";
import { connect } from "react-redux";
import Toolbar from "../../components/UI/Toolbar/Toolbar";
import { logoutUser } from "../../store/actions/userActions";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { push } from "react-router-redux";

import "./Layout.css";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
import Footer from "../../components/UI/Footer/Footer";

const theme = createMuiTheme({
	typography: {
		fontSize: 16,
		htmlFontSize: 14,
		fontFamily: ["Comfortaa", "cursive"],
	},
	palette: {
		primary: {
			light: "#895391",
			main: "#65446d",
			dark: "#895391",
			contrastText: "#fff",
		},
		secondary: {
			light: "#A5D6A7",
			main: "#ff7884",
			dark: "#2E7D32",
			contrastText: "#e2e2e2",
		},
	},
});

class Layout extends Component {

	render() {
		const { route, user, logout, children} = this.props;

		return (
			<MuiThemeProvider theme={theme}>
				<div
					className="layout"
					style={{ width: window.innerWidth, height: window.innerHeight }}
				>
					<header>
						<Toolbar
							route={route}
							user={user}
							logout={logout}
						/>
					</header>
					<main className="container">{children}</main>

					<NotificationContainer />
					<Footer />
				</div>
			</MuiThemeProvider>
		);
	}
}

const mapStateToProps = state => ({
	user: state.users.user
});

const mapDispatchToProps = dispatch => ({
	logout: () => dispatch(logoutUser()),
	route: path => dispatch(push(path)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Layout);
