import {
    ADD_NEW_SECTION_SUCCESS, ADD_QUESTION_SUCCESS, FETCH_ALL_SECTIONS_SUCCESS,
    TOGGLE_SECTION_FORM, GET_SECTION_BY_ID_SUCCESS, GET_FINISHED_SECTION_SUCCESS, GET_SECTION_BY_ID_FAILURE,
    EDIT_SECTION_FAILURE, ADD_NEW_SECTION_FAILURE, GET_FINISHED_SECTION_FAILURE, SUCCESS_SETTED_RATING_SECTION_FAILURE,
    DELETE_SECTION_FAILURE, FETCH_PLACE_BY_ID
} from "../actions/actionTypes";

export const initialState = {
    places: [],
    place: null,
    sectionById: null,
    showSectionForm: false,
    finishedSection: null,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NEW_SECTION_SUCCESS:
            return {
                ...state,
                places: state.places.concat(action.section),
                showSectionForm: false
            };
        case FETCH_PLACE_BY_ID:
            return {...state, place: action.place};
        case FETCH_ALL_SECTIONS_SUCCESS:
            return {...state, places: action.allSections};

        case TOGGLE_SECTION_FORM:
            return {...state, showSectionForm: !state.showSectionForm};

        case ADD_NEW_SECTION_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default reducer;