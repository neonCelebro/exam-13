const path = require('path');

const rootPath = __dirname;

let config = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: "places"
    },
    facebook: {
        appId: "2298700403749595",
        appSecret: "3db0f0d21666cb25ffdaceea6d7c89b6"
    },
    vkontakte: {
        appId: '6686206',
        appSecret: '6JxE3NN6ITZ9wQC6UXBH',
        redirectUrl: process.env.MODE === 'production' ? 'https://psyhology.ddns.net' : '//localhost:3000'
    }
};

module.exports = config;

