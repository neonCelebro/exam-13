import React, {Fragment} from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';
import UserMenu from "./UserMenu";
import IconButton from "@material-ui/core/IconButton/IconButton";
import LogOutIcon from "@material-ui/icons/ExitToAppOutlined";
import LogInIcon from "@material-ui/icons/MeetingRoom";
import AddIcon from "@material-ui/icons/AddCircle";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

const styles = {
    root: {
        flexGrow: 1,
        marginBottom: '30px'
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    link: {
        color: '#fff',
        fontSize: '16px',
        outline: 'none',
        textDecoration: 'none',
        '&:hover': {
            color: "#fff"
        }
    },
    enter: {
        color: '#ebebeb',
        '&:hover': {
            color: "#ebebeb",
            outline: 'none'
        },
        '&:isActive' : {
            color: "#ebebeb",
        },
        textDecoration: 'none'
    },
    icon: {
        fontSize: "50px",
        width: "25px",
        height: "25px"
    }
};

class MenuAppBar extends React.Component {

    state = {
        anchorEl: null,
        open: false,
    };


    handleClickOpen = (id) => {
        this.setState({open: true, openIs: id});
    };

    handleExit = () => {
        !this.props.user && this.props.route('/login');
        this.props.user && this.props.logout();
    };

    render() {
        const {classes, user} = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="static" color='primary'>
                    <Toolbar>
                        <Typography variant="title" style={{color: '#fff', fontSize: '14px'}} className={classes.flex}>
                            <Link to='/sections' className={classes.enter}>На главную</Link>
                        </Typography>

                                    <Typography
                                        variant="title"
                                        style={{color: '#fff', fontSize: '14px'}}>
                                        {this.props.user ? `Привет! ${this.props.user.displayName}` :
                                            <Link to='/login' className={classes.enter}>
                                                Войти
                                                <LogInIcon
                                                    style={{color: "#fff", margin: "0 10px", verticalAlign: 'middle'}}
                                                />
                                            </Link>}
                                    </Typography>

                        {this.props.user && (
                            <Fragment>
                                <UserMenu
                                    addHandleButton={this.handleClickOpen}
                                    avatar={this.props.user.avatar}
                                    route={this.props.route}
                                />
                                <Tooltip title="Добавить новое заведение">
                                    <Link to={"/addPlace"} className={classes.enter}>
                                    <IconButton
                                        id='create'
                                        aria-haspopup="true"
                                        color="inherit"
                                    >
                                        <AddIcon style={{fontSize: "35px"}} />
                                    </IconButton>
                                    </Link>
                                </Tooltip>
                            </Fragment>
                        )}
                        {user &&
                        <Tooltip title="Выйти">
                            <IconButton
                                id='menu-appbar'
                                aria-haspopup="true"
                                color="inherit"
                                onClick={this.handleExit}
                            >
                                <LogOutIcon  style={{fontSize: "35px"}} />
                            </IconButton>
                        </Tooltip>
                        }
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}


export default withStyles(styles)(MenuAppBar);
